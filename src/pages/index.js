import React from "react";
import { graphql } from "gatsby";
import { shape, string } from "prop-types";
import Layout from "../components/Layout";
import Scene from "../components/3D";
import AspectRatio from "../components/AspectRatio";

const IndexPage = ({ data }) => (
	<Layout siteMetadata={data.site.siteMetadata}>
		<AspectRatio>
			<Scene />
		</AspectRatio>
	</Layout>
);

export default IndexPage;

IndexPage.propTypes = {
	data: shape({
		site: shape({
			siteMetadata: shape({
				title: string.isRequired,
			}).isRequired,
		}).isRequired,
	}).isRequired,
};

export const query = graphql`
	query IndexPageQuery {
		site {
			siteMetadata {
				title
			}
		}
	}
`;
