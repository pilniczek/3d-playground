import React from "react";
import { graphql } from "gatsby";
import { shape, string } from "prop-types";
import Layout from "../components/Layout";

const NotFound = ({ data }) => (
	<Layout siteMetadata={data.site.siteMetadata}>
		<h1>404</h1>
	</Layout>
);

export default NotFound;

NotFound.propTypes = {
	data: shape({
		site: shape({
			siteMetadata: shape({
				title: string.isRequired,
			}).isRequired,
		}).isRequired,
	}).isRequired,
};

export const query = graphql`
	query NotFoundQuery {
		site {
			siteMetadata {
				title
			}
		}
	}
`;
