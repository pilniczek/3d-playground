import styled from "styled-components";

const StyledAspectRatio = styled.div`
	height: 0;
	padding-top: 66%;
	position: relative;

	.in {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
	}
`;

export default StyledAspectRatio;
