import React from "react";
import { node } from "prop-types";
import StyledAspectRatio from "./style";

const AspectRatio = ({ children, ...rest }) => {
	return (
		<StyledAspectRatio {...rest}>
			<div className="in">{children}</div>
		</StyledAspectRatio>
	);
};

AspectRatio.propTypes = {
	children: node.isRequired,
};

export default AspectRatio;
