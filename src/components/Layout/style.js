import styled from "styled-components";

const StyledLayout = styled.div`
	height: 100%;
	width: 100%;
	max-width: 800px;
	margin: 0 auto;
`;

export default StyledLayout;
