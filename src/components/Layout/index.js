import React from "react";
import { node } from "prop-types";
import GlobalStyle from "../_style";
import StyledLayout from "./style";

const Layout = ({ children, ...rest }) => {
	return (
		<>
			<GlobalStyle />
			<StyledLayout {...rest}>{children}</StyledLayout>
		</>
	);
};

Layout.propTypes = {
	children: node.isRequired,
};

export default Layout;
