import React from "react";
import { useLoader } from "react-three-fiber";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { string } from "prop-types";

const TinkerCadModel = ({ src, color, ...rest }) => {
	const { nodes } = useLoader(GLTFLoader, src);
	const { geometry, material } = nodes.Mesh_Mesh_head_geo001_lambert2SG001;

	return (
		<mesh visible geometry={geometry} {...rest}>
			<meshStandardMaterial attach="material" {...material} color={color} />
		</mesh>
	);
};

TinkerCadModel.propTypes = {
	color: string,
	src: string.isRequired,
};

TinkerCadModel.defaultProps = {
	color: undefined,
};

export default TinkerCadModel;
