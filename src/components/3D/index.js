import React, { Suspense, useState } from "react";
import { Canvas } from "react-three-fiber";
import Loading from "./Loading";
import TinkerCadModel from "./TinkerCadModel";

const Scene = () => {
	const [mousePos, setMousePos] = useState({ x: 0, y: 0 });

	const getMousePos = (e) => {
		const bounds = e.target.getBoundingClientRect();
		setMousePos({
			x: e.clientX - bounds.left,
			y: e.clientY - bounds.top,
		});
	};
	console.log("log mousePos: ", mousePos);
	return (
		<Canvas
			className="canvas"
			colorManagement
			onClick={(e) => getMousePos(e)}
			camera={{
				position: [20, 40, 50],
			}}
		>
			<ambientLight />
			<pointLight position={[10, 10, 10]} />
			<Suspense fallback={<Loading />}>
				<TinkerCadModel src="../3D-models/test-cube/full.glb" />
				<TinkerCadModel src="../3D-models/test-cube/empty.glb" />
			</Suspense>
		</Canvas>
	);
};

export default Scene;
